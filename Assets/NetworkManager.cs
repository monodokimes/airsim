﻿using UnityEngine;
using System.Collections;
using System;
using UnityStandardAssets.Characters.FirstPerson;

public class NetworkManager : MonoBehaviour {

    public Camera standbyCamera;
    public string versionString;

    SpawnSpot[] spawnSpots;

    void Start() {
        spawnSpots = FindObjectsOfType<SpawnSpot>();
        Connect();
    }

    void Connect() {
        PhotonNetwork.ConnectUsingSettings(versionString);
    }

    void OnGUI() {
        GUILayout.Label(PhotonNetwork.connectionStateDetailed.ToString());
    }

    void OnJoinedLobby() {
        Debug.Log("OnJoinedLobby");

        PhotonNetwork.JoinRandomRoom();
    }

    void OnPhotonRandomJoinFailed() {
        Debug.Log("OnPhotonRandomJoinFailed");

        PhotonNetwork.CreateRoom(null);
    }

    void OnJoinedRoom() {
        Debug.Log("OnJoinedRoom");

        SpawnMyPlayer();
    }

    void SpawnMyPlayer() {
        if (spawnSpots == null) {
            Debug.LogError("No spawn spots found");
            return;
        }

        SpawnSpot spawnSpot = spawnSpots[UnityEngine.Random.Range(0, spawnSpots.Length)];

        GameObject myPlayer = PhotonNetwork.Instantiate(
            "FPSController", 
            spawnSpot.transform.position, 
            spawnSpot.transform.rotation, 
            0);

        myPlayer.GetComponent<FirstPersonController>().enabled = true;
        myPlayer.GetComponent<CharacterController>().enabled = true;

        myPlayer.GetComponentInChildren<AudioListener>().enabled = true;
        myPlayer.GetComponentInChildren<Camera>().enabled = true;

        standbyCamera.enabled = false;
    }
}
