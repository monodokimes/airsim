﻿using UnityEngine;
using System.Collections;

public class NetworkCharacter : Photon.MonoBehaviour {

    Vector3 realPosition = Vector3.zero;
    Quaternion realRotation = Quaternion.identity;

    const int MESSAGES_PER_SECOND = 10;

    float updateSpeed = 0.3f;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        if (!photonView.isMine) {
            transform.position = Vector3.Lerp(transform.position, realPosition, updateSpeed);
            transform.rotation = Quaternion.Lerp(transform.rotation, realRotation, updateSpeed);
        }
	}

    void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo messageInfo) {

        if (stream.isWriting) {
            stream.SendNext(transform.position);
            stream.SendNext(transform.rotation);
        } else {
            realPosition = (Vector3)stream.ReceiveNext();
            realRotation = (Quaternion)stream.ReceiveNext();
        }
    }
}
